#!/bin/bash

# try to install from scrapinghub
sudo docker pull scrapinghub/splash

# add an alias
echo "alias splash=\"sudo docker run -it -p 8050:8050 --rm scrapinghub/splash\"" >> ~/.bashrc

# try a local installation and run
sudo docker run -it -p 8050:8050 --rm scrapinghub/splash
